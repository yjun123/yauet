### yauet (Yet Another UVC Extension Tool)

#### 介绍

简单的UVC 扩展通道调试脚本

#### 依赖

- uvcdynctrl

#### 使用方法

```bash
# 以 ROOT 执行

# 参数:        相机型号 命令 命令参数
$ sudo ./yauet WePalm   0xc6 0x02 #播放扫码音

```

### 新增相机设备

- 在 `camera_uvc_ext_ctrl` 函数中增加新的相机选项,

- 增加设备对应的命令处理操作


#### TODO

- 返回数据解析(?)

- 设备独立的命令集(?)

- 交互式控制 (?)
