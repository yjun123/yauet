#!/bin/bash
# Copyright (C), 2015-2023, Sunny OIT. CO., Ltd.
# Filename: yauet.sh
# Author: yanjun (zngyanj@sunnyoptical.com)
# Date: 2023/04/26
# Description: UVC 扩展通道调试脚本
# Usage: ./yauet.sh --help


UVC_MEDIA_DEFAULT_DEV=/dev/media0
UVC_TOOL="uvcdynctrl"

SUNNYPALM_VID="35c2"
SUNNYPALM_PID="0002"

#WePalm SunnyPalm
PALM_CAMERA_EXTENSION_UNIT_ID="2"
PALM_CAMERA_EXTENSION_UNIT_QUERY_CS="1"

PALM_CAMERA_EXTENSION_UNIT_QUERY_MAGIC="0x55aa"
PALM_CAMERA_EXTENSION_UNIT_QUERY_LEN="60"

PALM_CAMERA_XU_CMD_SET_UFU_MODE="0xE0"
PALM_CAMERA_XU_CMD_RESET_DEVICE="0x01"
PALM_CAMERA_XU_CMD_PLAY_SOUND="0xc6"
WEPALM_CAMERA_XU_CMD_RESET_PARAM="0x13"

#Mars05D
MARS05D_CAMERA_EXTENSION_UNIT_ID="2"
MARS05D_CAMERA_EXTENSION_UNIT_QUERY_CS="1"

UVC_CTRL_SET=0
UVC_CTRL_GET=1

LOG_LEVEL=1
function log_dbg(){
  content="[DBG]: $(date '+%Y-%m-%d %H:%M:%S')"
  [ $LOG_LEVEL -le 1  ] && echo -e "\033[34m"  ${content} "\033[0m $@"
}

function log_info(){
  content="[INFO]: $(date '+%Y-%m-%d %H:%M:%S')"
  [ $LOG_LEVEL -le 2  ] && echo -e "\033[32m"  ${content} "\033[0m $@"
}

function log_warn(){
  content="[WARN]: $(date '+%Y-%m-%d %H:%M:%S')"
  [ $LOG_LEVEL -le 3  ] && echo -e "\033[33m"  ${content} "\033[0m $@"
}

function log_err(){
  content="[ERR]: $(date '+%Y-%m-%d %H:%M:%S')"
  [ $LOG_LEVEL -le 4  ] && echo -e "\033[31m"  ${content} "\033[0m $@"
}


function delete_0x()
{
    var=$1
    echo ${var//0x/}
}

function uvc_query()
{

    local entity_id=$1
    local cs=$2
    local data=$3
    local method=$4
    
    if [ $method -eq $UVC_CTRL_SET ];then
        if ! ${UVC_TOOL} --set_raw="$entity_id:$cs" "(LE)$data" ;then
            log_err "uvc_query set error!"
            return 1
        fi
    else
        resp=$(${UVC_TOOL} --get_raw="$entity_id:$cs" "(LE)$data" | grep 'query current value of')
        if [ $? -eq 0 ];then
            resp=($resp)
            log_info "Show query result: ${resp[4]}"
        else
            log_err "uvc_query get error!"
            return 1
        fi
    fi

    return 0
}

function uvc_extension_query()
{
    if ! uvc_query "$g_ext_unit_id" "$g_ext_unit_cs" "$g_ext_unit_req" "$g_ext_unit_req_method";then
        log_err "uvc_extension_query error!"
        return 1
    fi

    return 0
}

function uvc_extension_query_set()
{
    g_ext_unit_req_method=$UVC_CTRL_SET
    log_dbg "uvc_extension_query_set"
    if ! uvc_extension_query;then
        return 1
    fi

    return 0;
}

function uvc_extension_query_get()
{
    g_ext_unit_req_method=$UVC_CTRL_GET
    log_dbg "uvc_extension_query_get"
    if ! uvc_extension_query;then
        return 1
    fi

    return 0;
}

function palm_camera_uvc_ext_ctrl()
{
    local cmd="$1"
    local data="$2"
    g_ext_unit_id="$PALM_CAMERA_EXTENSION_UNIT_ID"
    g_ext_unit_cs="$PALM_CAMERA_EXTENSION_UNIT_QUERY_CS"
    g_ext_unit_req="${PALM_CAMERA_EXTENSION_UNIT_QUERY_MAGIC}${cmd}ff${data}"
    
    log_dbg "origin: g_ext_unit_req  = $g_ext_unit_req"
    palm_camera_uvc_ext_req_format
    palm_camera_uvc_ext_req_checksum
    
    # sudo uvcdynctrl --set_raw=2:1 '(LE)0x55aaE00524554655000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000f8'
    if ! uvc_extension_query_set;then
        beep
    fi

    # if ! uvc_extension_query_get;then
    #     beep
    # fi

    return 0
}

function mars05d_camera_uvc_ext_ctrl()
{
    local cmd="$1"
    local data="$2"
    g_ext_unit_id="$MARS05D_CAMERA_EXTENSION_UNIT_ID"
    g_ext_unit_cs="$MARS05D_CAMERA_EXTENSION_UNIT_QUERY_CS"
    g_ext_unit_req="${cmd}${data}"
    
    palm_camera_uvc_ext_req_format
    # sudo uvcdynctrl --set_raw=2:1 '(LE)0x55aaE00524554655000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000f8'
    if ! uvc_extension_query_set;then
        beep
    fi

    # if ! uvc_extension_query_get;then
    #     beep
    # fi

    return 0
}

function palm_camera_uvc_ext_req_format()
{
    g_ext_unit_req=$(delete_0x "$g_ext_unit_req")

    len=${#g_ext_unit_req}
    g_ext_unit_req="0x${g_ext_unit_req}$(printf %0$((PALM_CAMERA_EXTENSION_UNIT_QUERY_LEN*2-len))d 0)"
    log_dbg "format: g_ext_unit_req = ${g_ext_unit_req}"
}


function palm_camera_uvc_ext_req_checksum()
{
    local checksum=0x00
    local byte
    g_ext_unit_req=$(delete_0x "$g_ext_unit_req")
    for byte in $(echo $g_ext_unit_req|sed 's/../& /g')
    do
        byte=0x$byte
        checksum=$((byte+checksum))
    done

    checksum=$(printf "0x%x" "$checksum")
    log_dbg "checksum of g_ext_unit_req = ${checksum}"

    local len=$((PALM_CAMERA_EXTENSION_UNIT_QUERY_LEN*2-2))
    g_ext_unit_req="0x${g_ext_unit_req:0:$len}${checksum:0-2:2}"

    log_dbg "checksum: g_ext_unit_req = ${g_ext_unit_req}"
}


function camera_uvc_ext_ctrl()
{
    local cam_type=${1,,}
    local cam_cmd=${2,,}
    local cam_data=${3,,}

    case $cam_type in
        "sunnypalm")
            if ! palm_camera_uvc_ext_ctrl $cam_cmd $cam_data;then
                return 1
            fi
        ;;
        "wepalm")
            if ! palm_camera_uvc_ext_ctrl $cam_cmd $cam_data   ;then
                return 1
            fi
        ;;
        "mars05d")
            if ! mars05d_camera_uvc_ext_ctrl $cam_cmd $cam_data   ;then
                return 1
            fi
        ;;
        *)
            log_err "Wrong camera type $cam_type"
            return 1
        ;;
    esac
            
    return 0
}

function is_meida_dev_existed()
{
    if [ ! -c "$UVC_MEDIA_DEFAULT_DEV" ];then
        log_err "$UVC_MEDIA_DEFAULT_DEV not found !"
        return 1
    fi
    
    log_dbg "$UVC_MEDIA_DEFAULT_DEV is existed."
    return 0
}

function beep()
{
    echo -n -e "\a"
}

function usage(){
	echo "Usage: $0 [cam_type] [cam_cmd] [cam_data]"
	echo "-h, --help	show usage"
    echo "cam_type: [SunnyPal, WePlam]"
    echo "cam_cmd: XU_CMD"
    echo "cam_data: XU_CMD_DATA"
    echo "example: sudo ./yauet WePalm 0xc6 0x02 #播放扫码音"
	exit 0
}

function main(){
    if [ ${#} -ne 3 ];then
        log_err "arguments error= $*"
        usage
        return -1
    fi

    local cam_type=${1}
    local cam_cmd=${2}
    local cam_data=${3}
    if is_meida_dev_existed;then
        #if ! camera_uvc_ext_ctrl "SunnyPalm" "$PALM_CAMERA_XU_CMD_RESET_DEVICE" 1;then
        if camera_uvc_ext_ctrl "${cam_type}" "${cam_cmd}" "${cam_data}";then
            log_info "UVC Extension Query cmd: $cam_cmd data: $cam_data to $cam_type successed."
        else
            log_err "UVC Extension Query cmd: $cam_cmd data: $cam_data to $cam_type failed."
            beep
        fi
    fi
}

if [ $UID -ne 0 ];then
  log_err "only root can do that!"
  exit
fi
main "$@"
